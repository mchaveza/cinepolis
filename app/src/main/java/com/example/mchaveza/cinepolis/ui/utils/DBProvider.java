package com.example.mchaveza.cinepolis.ui.utils;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.CONTENT_ITEM_TYPE;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.CONTENT_LIST_TYPE;

/**
 * Created by knock on 06/04/2017.
 */

public class DBProvider extends ContentProvider {

    /**
     * Constant for full access to table Complejos
     */
    private static final int COMPLEJOS = 100;

    /**
     * Constant for access to table Complejos by ID
     */
    private static final int COMPLEJOS_ID = 101;

    /**
     * Constant for full access to table PELICULA
     */
    private static final int PELICULAS = 110;

    /**
     * Constant for access to table Pelicula by ID
     */
    private static final int PELICULAS_ID = 111;

    /**
     * Constant for full access to table MULTIMEDIA
     */
    private static final int MULTIMEDIAS = 120;

    /**
     * Constant for access to table MULTIMEDIA by ID
     */
    private static final int MULTIMEDIA_ID = 121;

    /**
     * Constant for full access to table CARTELERA
     */
    private static final int CARTELERAS = 130;

    /**
     * Constant for access to table CARTELERA by ID
     */
    private static final int CARTELERA_ID = 131;


    /**
     * Static Instance of UriMatcher to match given URL
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        //Matcher of contents://com.example.mchaveza.cinepolis/cinepolis
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_COMPLEJOS, COMPLEJOS);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_PELICULAS, PELICULAS);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_MULTIMEDIA, MULTIMEDIAS);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_CARTELERA, CARTELERAS);

        //Matcher of contents://com.example.mchaveza.cinepolis/cinepolis/#
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_COMPLEJOS + "/#", COMPLEJOS_ID);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_PELICULAS + "/#", PELICULAS_ID);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_MULTIMEDIA + "/#", MULTIMEDIA_ID);
        sUriMatcher.addURI(DBContract.CONTENT_AUTHORITY, DBContract.PATH_CARTELERA + "/#", CARTELERA_ID);
    }

    //Instance of DBHelper
    private DBHelper mDbHelper = null;

    @Override
    public boolean onCreate() {
        mDbHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final int action = sUriMatcher.match(uri);

        switch (action) {
            case COMPLEJOS:
                cursor = db.query(DBContract.ComplejoEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
                break;
            case COMPLEJOS_ID:
                selection = DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(DBContract.ComplejoEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case PELICULAS:
                cursor = db.query(DBContract.PeliculaEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
                break;
            case PELICULAS_ID:
                selection = DBContract.PeliculaEntry.COLUMN_PELICULA_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(DBContract.PeliculaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MULTIMEDIAS:
                cursor = db.query(DBContract.MultimediaEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
                break;
            case MULTIMEDIA_ID:
                selection = DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(DBContract.MultimediaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case CARTELERAS:
                cursor = db.query(DBContract.CarteleraEntry.TABLE_NAME, projection, null, null, null, null, sortOrder);
                break;
            case CARTELERA_ID:
                selection = DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(DBContract.CarteleraEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("No match found for URI " + uri.toString());
        }

        //Notify the URI at the cursor to update the UI
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = sUriMatcher.match(uri);

        switch (match) {
            case COMPLEJOS:
                return DBContract.ComplejoEntry.CONTENT_LIST_TYPE;
            case COMPLEJOS_ID:
                return DBContract.ComplejoEntry.CONTENT_ITEM_TYPE;
            case PELICULAS:
                return DBContract.PeliculaEntry.CONTENT_LIST_TYPE;
            case PELICULAS_ID:
                return DBContract.PeliculaEntry.CONTENT_ITEM_TYPE;
            case MULTIMEDIAS:
                return DBContract.MultimediaEntry.CONTENT_LIST_TYPE;
            case MULTIMEDIA_ID:
                return DBContract.MultimediaEntry.CONTENT_ITEM_TYPE;
            case CARTELERAS:
                return DBContract.CarteleraEntry.CONTENT_LIST_TYPE;
            case CARTELERA_ID:
                return DBContract.CarteleraEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown uri " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}

package com.example.mchaveza.cinepolis.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.HorariosModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 10/04/2017.
 */

public class HorariosRecyclerAdapter extends RecyclerView.Adapter<HorariosRecyclerAdapter.ItemViewHolder> {

    private Context mContext;
    private List<HorariosModel> mList;

    public HorariosRecyclerAdapter(Context mContext, List<HorariosModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public HorariosRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_billboard, parent, false);

        return new HorariosRecyclerAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HorariosRecyclerAdapter.ItemViewHolder holder, final int position) {
        final HorariosModel model = mList.get(position);
        holder.day.setText(model.getFecha());
        holder.hour.setText(model.getHorario());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_billboard_day)
        TextView day;

        @BindView(R.id.item_billboard_hour)
        TextView hour;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
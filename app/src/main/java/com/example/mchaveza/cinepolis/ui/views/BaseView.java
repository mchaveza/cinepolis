package com.example.mchaveza.cinepolis.ui.views;

import android.content.Context;

/**
 * Created by knock on 01/04/2017.
 */

public interface BaseView {

    void showLoading();

    void hideLoading();

    Context getContext();
}

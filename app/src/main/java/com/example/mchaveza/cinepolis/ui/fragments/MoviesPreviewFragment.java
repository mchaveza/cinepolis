package com.example.mchaveza.cinepolis.ui.fragments;

import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ViewTarget;
import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.MultimediaModel;
import com.example.mchaveza.cinepolis.data.local.PeliculasDescripcionModel;
import com.example.mchaveza.cinepolis.ui.adapters.MultimediaRecyclerAdapter;
import com.example.mchaveza.cinepolis.ui.adapters.PeliculasRecyclerAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;
import com.example.mchaveza.cinepolis.ui.dialogs.OpenImageDialog;
import com.example.mchaveza.cinepolis.ui.utils.DBContract;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.example.mchaveza.cinepolis.ui.utils.DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ARCHIVO;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ID;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ID_PELICULA;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_TIPO;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_ACTORES;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_CLASIFICACION;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_DIRECTOR;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_DURACION;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_GENERO;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_ID;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_IMAGEN_CARTEL;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_SINOPSIS;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_TITULO;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesPreviewFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, MultimediaRecyclerAdapter.onEventClickListener {

    private static final int LOADER_ID = 3;

    public static MoviesPreviewFragment newInstance() {
        MoviesPreviewFragment fragment = new MoviesPreviewFragment();

        return fragment;
    }

    @BindView(R.id.movie_preview_recycler)
    RecyclerView mRecycler;

    MultimediaRecyclerAdapter mAdapter;
    List<MultimediaModel> list = new ArrayList<>();
    FullscreenVideoLayout videoLayout;
    OpenImageDialog openImageDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies_preview, container, false);
        videoLayout = (FullscreenVideoLayout) view.findViewById(R.id.videoview);
        openImageDialog = new OpenImageDialog();
        getLoaderManager().initLoader(LOADER_ID, null, this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = new String[]{
                COLUMN_MULTIMEDIA_ID,
                DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ID_PELICULA,
                DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ARCHIVO,
                DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_TIPO
        };

        return new CursorLoader(getActivity(), DBContract.MultimediaEntry.CONTENT_URI, projection, null, null, DBContract.MultimediaEntry.COLUMN_MULTIMEDIA_ID_PELICULA);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        list.clear();
        while (data.moveToNext()) {

            int idxId = data.getColumnIndex(COLUMN_MULTIMEDIA_ID);
            int idxIdPelicula = data.getColumnIndex(COLUMN_MULTIMEDIA_ID_PELICULA);
            int idxArchivo = data.getColumnIndex(COLUMN_MULTIMEDIA_ARCHIVO);
            int idxTipo = data.getColumnIndex(COLUMN_MULTIMEDIA_TIPO);

            MultimediaModel model = new MultimediaModel();
            if (data.getLong(idxIdPelicula) == MoviesTabbedFragment.id) {
                model.setId(data.getInt(idxId));
                model.setIdPelicula(data.getLong(idxIdPelicula));
                model.setArchivo(data.getString(idxArchivo));
                model.setTipo(data.getString(idxTipo));
                list.add(model);
                if (data.getString(idxTipo).equals("Video")) {
                    loadVideo(model.getArchivo());
                }
            }
        }
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (3 - position % 3);
            }
        });
        mRecycler.setLayoutManager(manager);
        mRecycler.setHasFixedSize(true);
        mAdapter = new MultimediaRecyclerAdapter(getActivity(), list, this);
        mRecycler.setAdapter(mAdapter);

    }

    private void loadVideo(String path) {
        videoLayout.setActivity(getActivity());
        Uri videoUri = Uri.parse("http://movil.cinepolis.com/Android/trailer/" + path + ".mp4");
        try {
            videoLayout.setVideoURI(videoUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onMoviesClick(String imagePath) {
        openImageDialog.setPath(imagePath, getActivity());
        openImageDialog.show(getFragmentManager(), OpenImageDialog.class.getName());

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}

package com.example.mchaveza.cinepolis.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.fragments.HomeFragment;
import com.example.mchaveza.cinepolis.ui.utils.SharedPreferencesConfiguration;

import java.util.List;

public class HomeActivity extends BaseActivity {

    private Boolean permissionGranted;
    private SharedPreferencesConfiguration sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = new SharedPreferencesConfiguration(HomeActivity.this);
        permissionGranted = sharedPreferences.getSharedPreferences();

        if (!permissionGranted) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        if(savedInstanceState == null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, HomeFragment.newInstance())
                    .commit();
        }
        mTitle.setText(getString(R.string.section_home));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    permissionGranted = true;
                    sharedPreferences.setSharedPreferences(permissionGranted);

                } else {

                    permissionGranted = false;
                    sharedPreferences.setSharedPreferences(permissionGranted);
                    Toast.makeText(HomeActivity.this, "Permission denied to access the GPS", Toast.LENGTH_SHORT).show();
                }
                return;
            }


        }
    }

}

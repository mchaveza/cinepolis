package com.example.mchaveza.cinepolis.data.net.events;

/**
 * Created by knock on 01/04/2017.
 */

public class GetComplexEvent {

    private int[] ids;

    public GetComplexEvent(int[] ids) {
        this.ids = ids;
    }

    public int[] getIds() {
        return ids;
    }

}

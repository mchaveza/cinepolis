package com.example.mchaveza.cinepolis.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.net.models.ComplexRequest;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;
import com.example.mchaveza.cinepolis.ui.fragments.HomeFragment;
import com.example.mchaveza.cinepolis.ui.presenters.ComplexPresenter;
import com.example.mchaveza.cinepolis.ui.views.ComplexView;

import java.util.List;

/**
 * Created by knock on 03/04/2017.
 */

public class SplashActivity extends AppCompatActivity implements ComplexView {

    private ComplexPresenter mPresenter;
    private ComplexRequest mRequest;

    LinearLayout progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progress = (LinearLayout) findViewById(R.id.splash_progress);
        mPresenter = new ComplexPresenter(this);
        mRequest = new ComplexRequest();
        mPresenter.getComplex(mRequest.buildUrlParameters());
        showLoading();
    }

    @Override
    public void showLoading() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void onGetComplex(List<ComplexResponse> response) {
        hideLoading();
        new HomeFragment().setResponse(response);
        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onGetComplexError(String error) {
        Intent intent = new Intent(SplashActivity.this, NoInternetActivity.class);
        startActivity(intent);
        finish();
    }
}

package com.example.mchaveza.cinepolis.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.PeliculasDescripcionModel;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesDescriptionFragment extends MoviesTabbedFragment{

    public static MoviesDescriptionFragment newInstance(){
        MoviesDescriptionFragment fragment = new MoviesDescriptionFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.container_movies_description, container, false);

        com.example.mchaveza.moviedescriptionmodule.models.PeliculasDescripcionModel model = new
                com.example.mchaveza.moviedescriptionmodule.models.PeliculasDescripcionModel();
        model.setImagenCartel(imagenCartel);
        model.setTitulo(titulo);
        model.setGenero(genero);
        model.setClasificacion(clasificacion);
        model.setDuracion(duracion);
        model.setId(id);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_description, new com.example.mchaveza.moviedescriptionmodule.fragments.MoviesDescriptionFragmentModule().newInstance(model))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_synopsis, new MoviesSynopsisFragment().newInstance())
                .commit();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}

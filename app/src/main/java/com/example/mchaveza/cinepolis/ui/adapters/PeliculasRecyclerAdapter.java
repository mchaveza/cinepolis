package com.example.mchaveza.cinepolis.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.PeliculasDescripcionModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 07/04/2017.
 */

public class PeliculasRecyclerAdapter extends RecyclerView.Adapter<PeliculasRecyclerAdapter.ItemViewHolder>{

    private Context mContext;
    private List<PeliculasDescripcionModel> mList;
    private PeliculasRecyclerAdapter.onEventClickListener mListener;


    public PeliculasRecyclerAdapter(Context mContext, List<PeliculasDescripcionModel> mList, PeliculasRecyclerAdapter.onEventClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public PeliculasRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movies, null);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final PeliculasDescripcionModel model = mList.get(position);
        Glide.with(mContext)
                .load(model.getImagenCartel())
                .into(holder.mMoviesImage);
        holder.mMoviesTitle.setText(model.getTitulo());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onMoviesClick(model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_movies_image)
        ImageView mMoviesImage;

        @BindView(R.id.item_movies_title)
        TextView mMoviesTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onEventClickListener {

        void onMoviesClick(PeliculasDescripcionModel model);

    }
}

package com.example.mchaveza.cinepolis.ui.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.ComplejosModel;
import com.example.mchaveza.cinepolis.ui.activities.ComplexActivity;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;
import com.example.mchaveza.cinepolis.ui.utils.DBContract;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_DIRECCION;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID_COMPLEJO_VISTA;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_LATITUD;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_LONGITUD;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_NOMBRE;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_TELEFONO;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.ComplejoEntry.COLUMN_COMPLEJO_URLSITIO;

/**
 * Created by knock on 31/03/2017.
 */

public class MapsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID = 1;

    private int markerPosition;
    private double averageLat;
    private double averageLong;

    private MapView mMapView;
    private GoogleMap mGoogleMap;

    public static String ARG_NOMBRE = "nombre";
    public static String ARG_ID_COMPLEJO_VISTA = "id_complejo_vista";

    private static List<ComplejosModel> list = new ArrayList<>();

    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = new String[]{
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID,
                COLUMN_COMPLEJO_NOMBRE,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID_COMPLEJO_VISTA,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_LATITUD,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_LONGITUD,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_TELEFONO,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_DIRECCION,
                DBContract.ComplejoEntry.COLUMN_COMPLEJO_URLSITIO

        };

        return new CursorLoader(getActivity(), DBContract.ComplejoEntry.CONTENT_URI, projection, null, null, DBContract.ComplejoEntry.COLUMN_COMPLEJO_ID);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        getDataFromCursor(data);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;
                LatLng[] locations = new LatLng[list.size()];
                averageLat = 0;
                averageLong = 0;

                for (int i = 0; i < list.size(); i++) {
                    averageLat += list.get(i).getLatitud();
                    averageLong += list.get(i).getLongitud();
                    locations[i] = new LatLng(list.get(i).getLatitud(), list.get(i).getLongitud());

                    mGoogleMap.addMarker(new MarkerOptions().position(locations[i]).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                }

                averageLat = (averageLat / list.size());
                averageLong = (averageLong / list.size());

                mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    final View customView = getActivity().getLayoutInflater().inflate(R.layout.custom_info_window, null);

                    @Override
                    public View getInfoWindow(Marker marker) {
                        render(marker, customView);
                        return customView;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        return null;
                    }

                    private void render(Marker marker, View view) {
                        TextView infoTitle = (TextView) customView.findViewById(R.id.info_title);
                        TextView infoAddress = (TextView) customView.findViewById(R.id.info_address);
                        TextView infoPhone = (TextView) customView.findViewById(R.id.info_phone);

                        markerPosition = getCurrentMarker(marker);
                        infoTitle.setText(list.get(markerPosition).getNombre());
                        infoAddress.setText(list.get(markerPosition).getDireccion());
                        infoPhone.setText(list.get(markerPosition).getTelefono());

                    }
                });

                customizeMapStyle();

                LatLng centerCamera = new LatLng(averageLat, averageLong);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(centerCamera).zoom(11).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent goToComplex = new Intent(getActivity(), ComplexActivity.class);
                        goToComplex.putExtra(ARG_NOMBRE, list.get(markerPosition).getNombre());
                        goToComplex.putExtra(ARG_ID_COMPLEJO_VISTA, list.get(markerPosition).getIdComplejoVista());
                        startActivity(goToComplex);
                    }
                });
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void getDataFromCursor(Cursor data) {
        list.clear();
        while (data.moveToNext()) {

            int idxId = data.getColumnIndex(COLUMN_COMPLEJO_ID);
            int idxIdComplejoVista = data.getColumnIndex(COLUMN_COMPLEJO_ID_COMPLEJO_VISTA);
            int idxLongitud = data.getColumnIndex(COLUMN_COMPLEJO_LONGITUD);
            int idxLatitud = data.getColumnIndex(COLUMN_COMPLEJO_LATITUD);
            int idxNombre = data.getColumnIndex(COLUMN_COMPLEJO_NOMBRE);
            int idxTelefono = data.getColumnIndex(COLUMN_COMPLEJO_TELEFONO);
            int idxDireccion = data.getColumnIndex(COLUMN_COMPLEJO_DIRECCION);
            int idxUrlSitio = data.getColumnIndex(COLUMN_COMPLEJO_URLSITIO);

            ComplejosModel model = new ComplejosModel();
            model.setId(data.getInt(idxId));
            model.setLongitud(data.getDouble(idxLongitud));
            model.setLatitud(data.getDouble(idxLatitud));
            model.setIdComplejoVista(data.getString(idxIdComplejoVista));
            model.setNombre(data.getString(idxNombre));
            model.setTelefono(data.getString(idxTelefono));
            model.setDireccion(data.getString(idxDireccion));
            model.setUrlSitio(data.getString(idxUrlSitio));

            list.add(model);
        }
    }

    private void customizeMapStyle() {
        try {
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.json));
            if (!success) {
                Log.e("Error", "Style parsing failed");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Error", "Can't find style");
        }
    }

    private int getCurrentMarker(Marker marker) {
        int markerPosition = 0;
        for (int i = 0; i < list.size(); i++) {
            if (marker.getPosition().latitude == list.get(i).getLatitud()) {
                if (marker.getPosition().longitude == list.get(i).getLongitud()) {
                    markerPosition = i;
                }
            }
        }
        return markerPosition;
    }
}
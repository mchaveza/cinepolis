package com.example.mchaveza.cinepolis.data.net.interfaces;

import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by knock on 01/04/2017.
 */

public interface ComplexInterface {

    @GET("/Consumo.svc/json/ObtenerCiudadesPaises")
    Call<List<ComplexResponse>> getLista(
            @Query("idsPaises") int[] idsPaises);

}

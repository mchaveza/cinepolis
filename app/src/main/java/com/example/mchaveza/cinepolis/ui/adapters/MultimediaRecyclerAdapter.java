package com.example.mchaveza.cinepolis.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.MultimediaModel;
import com.example.mchaveza.cinepolis.data.local.PeliculasDescripcionModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 09/04/2017.
 */

public class MultimediaRecyclerAdapter extends RecyclerView.Adapter<MultimediaRecyclerAdapter.ItemViewHolder>{

    private String imagePath;
    private Context mContext;
    private List<MultimediaModel> mList;
    private MultimediaRecyclerAdapter.onEventClickListener mListener;

    public MultimediaRecyclerAdapter(Context mContext, List<MultimediaModel> mList, MultimediaRecyclerAdapter.onEventClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mListener = mListener;
    }

    @Override
    public MultimediaRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_multimedia, null);

        return new MultimediaRecyclerAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MultimediaRecyclerAdapter.ItemViewHolder holder, final int position) {
        final MultimediaModel model = mList.get(position);
        imagePath = "http://cinepolis.com/_MOVIL/Android/galeria/thumb/"+model.getArchivo();
        Glide.with(mContext)
                .load(imagePath)
                .into(holder.mImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onMoviesClick("http://cinepolis.com/_MOVIL/Android/galeria/thumb/"+model.getArchivo());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_multimedia_image)
        ImageView mImage;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onEventClickListener {

        void onMoviesClick(String imagePath);

    }
}

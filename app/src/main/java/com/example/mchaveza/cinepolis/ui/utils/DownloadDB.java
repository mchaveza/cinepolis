package com.example.mchaveza.cinepolis.ui.utils;

import android.content.Context;

import com.example.mchaveza.cinepolis.R;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;

/**
 * Created by knock on 04/04/2017.
 */

public class DownloadDB {

    private onDownloadDBListener mListener;
    private Context mContext;
    private String mUrl;

    public DownloadDB(onDownloadDBListener listener, Context context, String url) {
        mListener = listener;
        mContext = context;
        mUrl = url;
        download();
    }

    private void download() {
        String path = mContext.getString(R.string.database_path);
        File file = new File(path);
        if(!file.exists()){
            file.mkdir();
        }
        Ion.with(mContext)
                .load(mUrl)
                .noCache()
                .write(new File(path+"/cinepolis.db"))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {
                        mListener.onDownloadDB();

                    }
                });

    }

    public interface onDownloadDBListener {
        void onDownloadDB();
    }
}

package com.example.mchaveza.cinepolis.ui.activities;

import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.fragments.MoviesShowtimesFragment;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesShowtimesActivity extends BaseActivity{



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, MoviesShowtimesFragment.newInstance())
                    .commit();
        }
        mTitle.setText(getString(R.string.section_movies_showtimes));
    }
}

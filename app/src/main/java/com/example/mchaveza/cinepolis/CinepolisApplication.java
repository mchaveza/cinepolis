package com.example.mchaveza.cinepolis;

import android.app.Application;

import com.example.mchaveza.cinepolis.data.net.MVPManager;
import com.example.mchaveza.cinepolis.data.net.base.BusProvider;
import com.squareup.otto.Bus;

/**
 * Created by knock on 31/03/2017.
 */

public class CinepolisApplication extends Application {

    private MVPManager mManager;
    private Bus mBus = BusProvider.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        mManager = new MVPManager(this, mBus);
        mBus.register(mManager);

    }


}

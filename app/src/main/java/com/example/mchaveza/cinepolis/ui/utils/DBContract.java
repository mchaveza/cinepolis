package com.example.mchaveza.cinepolis.ui.utils;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by knock on 06/04/2017.
 */

public class DBContract {

    /**
     * My package for content authority
     */
    public static final String CONTENT_AUTHORITY = "com.example.mchaveza.cinepolis";

    /**
     * BASE URI for cities
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    //Name of Complejo's table
    public static final String PATH_COMPLEJOS = "Complejo";

    //Name of Pelicula's table
    public static final String PATH_PELICULAS = "Pelicula";

    //Name of Multimedia's table
    public static final String PATH_MULTIMEDIA = "Multimedia";

    //Name of Cartelera's table
    public static final String PATH_CARTELERA = "Cartelera";

    public static final class ComplejoEntry implements BaseColumns{

        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_COMPLEJOS;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_COMPLEJOS;

        //Content Uri for access to Complejo's Table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_COMPLEJOS);

        //The table's name
        public static final String TABLE_NAME = "Complejo";

        //Table's ID
        public static final String COLUMN_COMPLEJO_ID = "Id";

        //The rest of the columns from Complejo's table
        public static final String COLUMN_COMPLEJO_ID_COMPLEJO_VISTA = "IdComplejoVista";
        public static final String COLUMN_COMPLEJO_NOMBRE = "Nombre";
        public static final String COLUMN_COMPLEJO_LATITUD = "Latitud";
        public static final String COLUMN_COMPLEJO_LONGITUD = "Longitud";
        public static final String COLUMN_COMPLEJO_TELEFONO = "Telefono";
        public static final String COLUMN_COMPLEJO_DIRECCION = "Direccion";
        public static final String COLUMN_COMPLEJO_URLSITIO = "UrlSitio";

    }

    public static final class PeliculaEntry implements BaseColumns{

        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_PELICULAS;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_PELICULAS;

        //Content Uri for access to Pelicula's Table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PELICULAS);

        //Table's name
        public static final String TABLE_NAME = "Pelicula";

        //Table's ID
        public static final String COLUMN_PELICULA_ID = "Id";

        //The rest of the columns from Pelicula's table
        public static final String COLUMN_PELICULA_TITULO = "Titulo";
        public static final String COLUMN_PELICULA_GENERO = "Genero";
        public static final String COLUMN_PELICULA_CLASIFICACION = "Clasificacion";
        public static final String COLUMN_PELICULA_DURACION = "Duracion";
        public static final String COLUMN_PELICULA_DIRECTOR = "Director";
        public static final String COLUMN_PELICULA_ACTORES = "Actores";
        public static final String COLUMN_PELICULA_SINOPSIS = "Sinopsis";
        public static final String COLUMN_PELICULA_IMAGEN_CARTEL = "ImagenCartel";

    }

    public static final class MultimediaEntry implements BaseColumns{

        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_MULTIMEDIA;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_MULTIMEDIA;

        //Content Uri for access to Pelicula's Table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_MULTIMEDIA);

        //Table's name
        public static final String TABLE_NAME = "Multimedia";

        //Table's ID
        public static final String COLUMN_MULTIMEDIA_ID = "Id";

        //The rest of the columns from Multimedia's table
        public static final String COLUMN_MULTIMEDIA_ID_PELICULA = "IdPelicula";
        public static final String COLUMN_MULTIMEDIA_ARCHIVO = "Archivo";
        public static final String COLUMN_MULTIMEDIA_TIPO = "Tipo";

    }

    public static final class CarteleraEntry implements BaseColumns{

        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_CARTELERA;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/"
                + CONTENT_AUTHORITY + "/" + PATH_CARTELERA;

        //Content Uri for access to Pelicula's Table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_CARTELERA);

        //Table's name
        public static final String TABLE_NAME = "Cartelera";

        //Table's ID
        public static final String COLUMN_CARTELERA_ID = "Id";

        //The rest of the columns from Cartelera's table
        public static final String COLUMN_CARTELERA_ID_PELICULA = "IdPelicula";
        public static final String COLUMN_CARTELERA_ID_COMPLEJO_VISTA = "IdComplejoVista";
        public static final String COLUMN_CARTELERA_HORARIO = "Horario";
        public static final String COLUMN_CARTELERA_FECHA = "Fecha";

    }
}

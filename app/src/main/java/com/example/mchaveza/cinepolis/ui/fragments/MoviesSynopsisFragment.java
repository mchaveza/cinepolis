package com.example.mchaveza.cinepolis.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mchaveza.cinepolis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 10/04/2017.
 */

public class MoviesSynopsisFragment extends Fragment {

    public static MoviesSynopsisFragment newInstance(){
        MoviesSynopsisFragment fragment = new MoviesSynopsisFragment();

        return fragment;
    }

    TextView director;
    TextView cast;
    TextView synopsis;
    LinearLayout directorIndicator;
    LinearLayout castIndicator;
    TextView synopsisIndicator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies_description_synopsis, container, false);
        director = (TextView) view.findViewById(R.id.movie_description_director);
        cast = (TextView) view.findViewById(R.id.movie_description_cast);
        synopsis = (TextView) view.findViewById(R.id.movie_description_synopsis);
        directorIndicator = (LinearLayout) view.findViewById(R.id.movies_desciption_director_indicator);
        castIndicator = (LinearLayout) view.findViewById(R.id.movie_description_cast_indicator);
        synopsisIndicator = (TextView) view.findViewById(R.id.movie_description_synopsis_indicator);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!MoviesTabbedFragment.director.isEmpty() && !MoviesTabbedFragment.director.equals(" ")){
            directorIndicator.setVisibility(View.VISIBLE);
            director.setText(MoviesTabbedFragment.director);
        }

        if(!MoviesTabbedFragment.actores.isEmpty() && !MoviesTabbedFragment.actores.equals(" ")){
            castIndicator.setVisibility(View.VISIBLE);
            cast.setText(MoviesTabbedFragment.actores);
        }

        if(!MoviesTabbedFragment.sinopsis.isEmpty() && !MoviesTabbedFragment.actores.equals(" ")){
            synopsisIndicator.setVisibility(View.VISIBLE);
            synopsis.setText(MoviesTabbedFragment.sinopsis);
        }
    }
}

package com.example.mchaveza.cinepolis.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;
import com.example.mchaveza.cinepolis.ui.activities.MapsActivity;
import com.example.mchaveza.cinepolis.ui.adapters.ComplejosRecyclerAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;
import com.example.mchaveza.cinepolis.ui.utils.DownloadDB;
import com.example.mchaveza.cinepolis.ui.utils.GPSTracker;
import com.example.mchaveza.cinepolis.ui.views.BaseView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by knock on 31/03/2017.
 */

public class HomeFragment extends BaseFragment implements BaseView, ComplejosRecyclerAdapter.onEventClickListener, DownloadDB.onDownloadDBListener {

    private GPSTracker gps;
    public static double userLatitude;
    public static double userLongitude;

    private static List<ComplexResponse> mResponse;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @BindView(R.id.home_recycler)
    RecyclerView mRecycler;

    @BindView(R.id.home_search)
    EditText mSearch;

    LinearLayout mProgress;
    LinearLayout userAdvise;

    ComplejosRecyclerAdapter mAdapter;
    List<ComplexResponse> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mProgress = (LinearLayout) view.findViewById(R.id.home_progress);
        userAdvise = (LinearLayout) view.findViewById(R.id.home_advice_message);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.setHasFixedSize(true);
        mAdapter = new ComplejosRecyclerAdapter(getActivity(), list, this);
        mRecycler.setAdapter(mAdapter);

        search();
    }

    @OnClick(R.id.home_location)
    public void onClickMyLocation() {
        getMyLocation();
    }

    @Override
    public void showLoading() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onDetailsMap(ComplexResponse model) {
        showLoading();
        String url = String.format("http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=%s", model.getId());
        new DownloadDB(this, getActivity(), url);
    }

    @Override
    public void onDownloadDB() {
        Intent goToMaps = new Intent(getActivity(), MapsActivity.class);
        startActivity(goToMaps);
        hideLoading();
    }

    public void search() {
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                list.clear();
                for (int index = 0; index < mResponse.size(); index++) {
                    if (charSequence.toString().equalsIgnoreCase(mResponse.get(index).getPais()) || charSequence.toString().equalsIgnoreCase(mResponse.get(index).getEstado()) || charSequence.toString().toLowerCase().equals(mResponse.get(index).getUris())) {
                        list.add(mResponse.get(index));
                        HomeFragment.this.userAdvise.setVisibility(View.GONE);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void setResponse(List<ComplexResponse> response) {
        mResponse = response;
    }

    private void getMyLocation() {
        try {
            gps = new GPSTracker(getActivity());
            if (gps.canGetLocation()) {
                userLatitude = gps.getLatitude();
                userLongitude = gps.getLongitude();

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = null;
                String extractCity = "";
                addresses = geocoder.getFromLocation(userLatitude, userLongitude, 1);
                if (addresses.size() > 0) {
                    String cityName = addresses.get(0).getAddressLine(2);
                    for (int i = 6; cityName.charAt(i) != ','; i++) {
                        extractCity += String.valueOf(cityName.charAt(i));
                    }

                    for (int i = 0; i < mResponse.size(); i++) {
                        if (extractCity.toString().equalsIgnoreCase(mResponse.get(i).getUris())) {
                            final int position = i;

                            new AlertDialog.Builder(getActivity())
                                    .setTitle("My location")
                                    .setMessage("Location detected: " + extractCity + "\nWould you like to continue?")
                                    .setPositiveButton(getString(R.string.dialog_location_btn_ok), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            showLoading();
                                            String url = String.format("http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=%s", mResponse.get(position).getId());
                                            new DownloadDB(HomeFragment.this, getActivity(), url);
                                        }
                                    })
                                    .setNegativeButton(getString(R.string.dialog_location_btn_cancel), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_map)
                                    .show();
                        }
                    }
                }
            } else {
                gps.showSettingsAlert();
            }
        } catch (Exception e) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Location Error")
                    .setMessage("Sorry, service unavailable for your current location...")
                    .setPositiveButton("Got it", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.stat_notify_error)
                    .show();
        }
    }
}
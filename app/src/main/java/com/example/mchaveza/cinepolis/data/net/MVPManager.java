package com.example.mchaveza.cinepolis.data.net;

import android.content.Context;
import android.util.Log;

import com.example.mchaveza.cinepolis.data.net.callbacks.ComplexCallback;
import com.example.mchaveza.cinepolis.data.net.events.GetComplexEvent;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by knock on 01/04/2017.
 */

public class MVPManager {

    private static final String TAG = MVPManager.class.getSimpleName();
    private Context mContext;
    private Bus mBus;
    private MVPClient mClient;

    public MVPManager(Context mContext, Bus mBus) {
        this.mContext = mContext;
        this.mBus = mBus;
        try{
            mClient = MVPClient.getInstance(mContext);
        }catch(ClassNotFoundException e){
            Log.e(TAG, e.getMessage());
        }
    }

    @Subscribe
    public void onGetComplex(GetComplexEvent event){
        mClient.getComplex(event.getIds(), ComplexCallback.complexCallback(mContext));
    }
}

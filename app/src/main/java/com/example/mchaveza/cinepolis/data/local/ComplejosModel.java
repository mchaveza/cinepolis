package com.example.mchaveza.cinepolis.data.local;

/**
 * Created by knock on 31/03/2017.
 */

public class ComplejosModel {

    int Id;
    double Latitud;
    double Longitud;
    String IdComplejoVista;
    String Nombre;
    String Telefono;
    String Direccion;
    String UrlSitio;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getLatitud() {
        return Latitud;
    }

    public void setLatitud(double latitud) {
        Latitud = latitud;
    }

    public double getLongitud() {
        return Longitud;
    }

    public String getIdComplejoVista() {
        return IdComplejoVista;
    }

    public void setIdComplejoVista(String idComplejoVista) {
        IdComplejoVista = idComplejoVista;
    }

    public void setLongitud(double longitud) {
        Longitud = longitud;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getUrlSitio() {
        return UrlSitio;
    }

    public void setUrlSitio(String urlSitio) {
        UrlSitio = urlSitio;
    }
}

package com.example.mchaveza.cinepolis.data.local;

/**
 * Created by knock on 10/04/2017.
 */

public class HorariosModel {

    long idPelicula;
    String idComplejoVista;
    String horario;
    String fecha;

    public long getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(long idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getIdComplejoVista() {
        return idComplejoVista;
    }

    public void setIdComplejoVista(String idComplejoVista) {
        this.idComplejoVista = idComplejoVista;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}

package com.example.mchaveza.cinepolis.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.mchaveza.cinepolis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 09/04/2017.
 */

public class OpenImageDialog extends DialogFragment {

    public static String path;
    public static Context mContext;

    @BindView(R.id.open_image_closeDialog)
    ImageView close;

    @BindView(R.id.open_image)
    ImageView hugeImage;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_open_image, null);

        ButterKnife.bind(this, dialogView);

        return new AlertDialog.Builder(getContext())
                .setView(dialogView)
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Glide.with(mContext)
                .load(path)
                .into(hugeImage);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setPath(String path, Context context) {
        this.path = path;
        mContext = context;
    }

}

package com.example.mchaveza.cinepolis.data.net.events;

import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;

import java.util.List;

/**
 * Created by knock on 01/04/2017.
 */

public class SendComplexEvent {

    private boolean isSuccess;
    private String message;
    private List<ComplexResponse> response;

    public SendComplexEvent(boolean isSuccess, String message, List<ComplexResponse> response) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.response = response;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public List<ComplexResponse> getResponse() {
        return response;
    }
}

package com.example.mchaveza.cinepolis.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by knock on 31/03/2017.
 */

public class ComplejosRecyclerAdapter extends RecyclerView.Adapter<ComplejosRecyclerAdapter.ItemViewHolder> {

    private Context mContext;
    private List<ComplexResponse> mList;
    private onEventClickListener mListener;

    public ComplejosRecyclerAdapter(Context context, List<ComplexResponse> list, onEventClickListener listener) {
        mContext = context;
        mList = list;
        mListener = listener;
    }

    @Override
    public ComplejosRecyclerAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ComplejosRecyclerAdapter.ItemViewHolder holder, int position) {
        final ComplexResponse model = mList.get(position);
        holder.mPais.setText(model.getPais());
        holder.mEstado.setText(model.getEstado());
        holder.mUris.setText(model.getUris());

        switch (model.getPais()) {
            case ("México"):
                Glide.with(mContext)
                        .load("http://www.inside-mexico.com/wp-content/uploads/2015/02/bandera1934.png")
                        .into(holder.mBandera);
                break;
            case ("Colombia"):
                Glide.with(mContext)
                        .load("https://upload.wikimedia.org/wikipedia/commons/f/f8/Flag_of_Colombia.png")
                        .into(holder.mBandera);
                break;
            case ("Guatemala"):
                Glide.with(mContext)
                        .load("http://vignette3.wikia.nocookie.net/coasterpedia/images/5/55/Guatemala.png/revision/latest?cb=20121110220656")
                        .into(holder.mBandera);
                break;
            case ("Costa Rica"):
                Glide.with(mContext)
                        .load("http://vignette2.wikia.nocookie.net/lossimpson/images/5/53/Bandera_de_Costa_Rica.png/revision/latest?cb=20091019004557&path-prefix=es")
                        .into(holder.mBandera);
                break;
            case ("Perú"):
                Glide.with(mContext)
                        .load("http://vignette2.wikia.nocookie.net/zelda/images/7/71/Per%C3%BA.png/revision/latest?cb=20101213181806&path-prefix=es")
                        .into(holder.mBandera);
                break;
            case ("Venezuela"):
                Glide.with(mContext)
                        .load("http://vignette2.wikia.nocookie.net/ssbb/images/7/7d/Bandera_de_Venezuela.png/revision/latest?cb=20100727062950&path-prefix=es")
                        .into(holder.mBandera);
                break;
            case ("Panamá"):
                Glide.with(mContext)
                        .load("http://vignette1.wikia.nocookie.net/lossimpson/images/f/ff/Bandera_de_Panam%C3%A1.png/revision/latest?cb=20091210170221&path-prefix=es")
                        .into(holder.mBandera);
                break;
            case ("El Salvador"):
                Glide.with(mContext)
                        .load("https://s-media-cache-ak0.pinimg.com/originals/f5/ea/2b/f5ea2b57cdb3b89f880dfad4be45b5af.jpg")
                        .into(holder.mBandera);
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDetailsMap(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.home_bandera)
        CircleImageView mBandera;

        @BindView(R.id.home_estado)
        TextView mEstado;

        @BindView(R.id.home_uris)
        TextView mUris;

        @BindView(R.id.home_pais)
        TextView mPais;

        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onEventClickListener {
        void onDetailsMap(ComplexResponse model);
    }

}

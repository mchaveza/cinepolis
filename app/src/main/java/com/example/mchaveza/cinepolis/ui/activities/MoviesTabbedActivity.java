package com.example.mchaveza.cinepolis.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.adapters.SectionsPageAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.fragments.MoviesTabbedFragment;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesTabbedActivity extends BaseActivity {

    private static String ARG_MOVIE_TITLE = "movie_title";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intentFromComplex = getIntent();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, MoviesTabbedFragment.newInstance(intentFromComplex))
                    .commit();
        }
        mTitle.setText(intentFromComplex.getStringExtra(ARG_MOVIE_TITLE));
        mTitle.setTextSize(getResources().getDimension(R.dimen.description_showtime_title_text_size));
    }
}
package com.example.mchaveza.cinepolis.data.net.base;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by knock on 01/04/2017.
 */

public class BusProvider {

    public static final Bus mBus = new Bus(ThreadEnforcer.ANY);

    public static Bus getInstance() {
        return mBus;
    }

    public BusProvider() {
    }
}

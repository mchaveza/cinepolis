package com.example.mchaveza.cinepolis.ui.views;

import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;

import java.util.List;

/**
 * Created by knock on 01/04/2017.
 */

public interface ComplexView extends BaseView {

    void onGetComplex(List<ComplexResponse> response);

    void onGetComplexError(String error);

}

package com.example.mchaveza.cinepolis.ui.presenters;

import com.example.mchaveza.cinepolis.data.net.base.BusProvider;
import com.squareup.otto.Bus;

/**
 * Created by knock on 01/04/2017.
 */

public class BasePresenter {

    public static final Bus mBus = BusProvider.getInstance();

    public BasePresenter(){

    }

    public void register(){
        mBus.register(this);
    }

}

package com.example.mchaveza.cinepolis.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.utils.SharedPreferencesConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by knock on 31/03/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public TextView mTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mTitle = (TextView) findViewById(R.id.toolbar_title);

        ImageView goBack = (ImageView) findViewById(R.id.toolbar_go_back);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }
}

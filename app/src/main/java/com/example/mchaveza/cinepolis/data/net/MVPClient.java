package com.example.mchaveza.cinepolis.data.net;

import android.content.Context;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.net.interfaces.ComplexInterface;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by knock on 01/04/2017.
 */

public class MVPClient {

    private final Retrofit mRetrofit;
    private Context mContext;
    private static MVPClient mClient;

    public static MVPClient getInstance(Context context) throws ClassNotFoundException{
        if(mClient == null){
            mClient = new MVPClient(context);
        }
        return mClient;
    }

    public MVPClient(Context context){
        mContext = context;
        mRetrofit = new Retrofit.Builder()
                .baseUrl(mContext.getString(R.string.endpoint))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void getComplex(int[] ids, Callback<List<ComplexResponse>> callback){
        ComplexInterface complexInterface = mRetrofit.create(ComplexInterface.class);
        complexInterface.getLista(ids).enqueue(callback);
    }
}
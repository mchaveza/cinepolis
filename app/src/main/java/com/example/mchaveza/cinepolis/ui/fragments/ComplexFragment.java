package com.example.mchaveza.cinepolis.ui.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.HorariosModel;
import com.example.mchaveza.cinepolis.data.local.PeliculasDescripcionModel;
import com.example.mchaveza.cinepolis.ui.activities.MoviesTabbedActivity;
import com.example.mchaveza.cinepolis.ui.adapters.PeliculasRecyclerAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;
import com.example.mchaveza.cinepolis.ui.utils.DBContract;
import com.example.mchaveza.cinepolis.ui.utils.DBHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_ACTORES;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_CLASIFICACION;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_DIRECTOR;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_DURACION;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_GENERO;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_ID;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_IMAGEN_CARTEL;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_SINOPSIS;
import static com.example.mchaveza.cinepolis.ui.utils.DBContract.PeliculaEntry.COLUMN_PELICULA_TITULO;

/**
 * Created by knock on 03/04/2017.
 */

public class ComplexFragment extends BaseFragment implements PeliculasRecyclerAdapter.onEventClickListener {

    private static String ARG_MOVIE_ID = "movie_id";
    private static String ARG_MOVIE_TITLE = "movie_title";
    private static String ARG_MOVIE_GENRE = "movie_genre";
    private static String ARG_MOVIE_CLASIFICATION = "movie_clasification";
    private static String ARG_MOVIE_DURATION = "movie_duration";
    private static String ARG_MOVIE_DIRECTOR = "movie_director";
    private static String ARG_MOVIE_CAST = "movie_cast";
    private static String ARG_MOVIE_SYNOPSIS = "movie_synopsis";
    private static String ARG_MOVIE_IMAGE = "movie_image";
    public static String ARG_ID_COMPLEJO_VISTA = "id_complejo_vista";

    private static String idComplejoVista;
    private DBHelper mDbHelper;

    public static ComplexFragment newInstance(String id) {
        ComplexFragment fragment = new ComplexFragment();
        idComplejoVista = id;
        return fragment;
    }

    @BindView(R.id.complex_recycler)
    RecyclerView mRecycler;

    PeliculasRecyclerAdapter mAdapter;
    List<PeliculasDescripcionModel> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_complex, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDbHelper = new DBHelper(getActivity());
        executeRawQuery();
    }

    @Override
    public void onMoviesClick(PeliculasDescripcionModel model) {
        Intent goToTabbed = new Intent(getActivity(), MoviesTabbedActivity.class);
        goToTabbed.putExtra(ARG_MOVIE_ID, model.getId());
        goToTabbed.putExtra(ARG_MOVIE_TITLE, model.getTitulo());
        goToTabbed.putExtra(ARG_MOVIE_GENRE, model.getGenero());
        goToTabbed.putExtra(ARG_MOVIE_CLASIFICATION, model.getClasificacion());
        goToTabbed.putExtra(ARG_MOVIE_DURATION, model.getDuracion());
        goToTabbed.putExtra(ARG_MOVIE_DIRECTOR, model.getDirector());
        goToTabbed.putExtra(ARG_MOVIE_CAST, model.getActores());
        goToTabbed.putExtra(ARG_MOVIE_SYNOPSIS, model.getSinopsis());
        goToTabbed.putExtra(ARG_MOVIE_IMAGE, model.getImagenCartel());
        goToTabbed.putExtra(ARG_ID_COMPLEJO_VISTA, idComplejoVista);
        startActivity(goToTabbed);
    }

    public void executeRawQuery() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String query = "select distinct Pelicula.Id, Pelicula.Titulo, Pelicula.Genero, Pelicula.Clasificacion, Pelicula.Duracion, Pelicula.Director, Pelicula.Actores, Pelicula.Sinopsis, Pelicula.ImagenCartel from Pelicula, Cartelera where Cartelera.IdPelicula = Pelicula.Id and Cartelera.IdComplejoVista='"+idComplejoVista+"' GROUP BY Pelicula.Titulo";
        Cursor data = db.rawQuery(query, null);
        while (data.moveToNext()) {

            int idxId = data.getColumnIndex(COLUMN_PELICULA_ID);
            int idxTitulo = data.getColumnIndex(COLUMN_PELICULA_TITULO);
            int idxGenero = data.getColumnIndex(COLUMN_PELICULA_GENERO);
            int idxClasificacion = data.getColumnIndex(COLUMN_PELICULA_CLASIFICACION);
            int idxDuracion = data.getColumnIndex(COLUMN_PELICULA_DURACION);
            int idxDirector = data.getColumnIndex(COLUMN_PELICULA_DIRECTOR);
            int idxActores = data.getColumnIndex(COLUMN_PELICULA_ACTORES);
            int idxSinopsis = data.getColumnIndex(COLUMN_PELICULA_SINOPSIS);
            int idxImagenCartel = data.getColumnIndex(COLUMN_PELICULA_IMAGEN_CARTEL);

            PeliculasDescripcionModel model = new PeliculasDescripcionModel();

            model.setId(data.getInt(idxId));
            model.setTitulo(data.getString(idxTitulo));
            model.setGenero(data.getString(idxGenero));
            model.setClasificacion(data.getString(idxClasificacion));
            model.setDuracion(data.getString(idxDuracion));
            model.setDirector(data.getString(idxDirector));
            model.setActores(data.getString(idxActores));
            model.setSinopsis(data.getString(idxSinopsis));
            model.setImagenCartel("http://cinepolis.com/_MOVIL/Android/cartel/xxhdpi/" + data.getString(idxImagenCartel));

            list.add(model);
        }
        mRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mRecycler.setHasFixedSize(true);
        mAdapter = new PeliculasRecyclerAdapter(getActivity(), list, this);
        mRecycler.setAdapter(mAdapter);
    }


}

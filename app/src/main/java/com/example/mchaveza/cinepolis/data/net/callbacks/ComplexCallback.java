package com.example.mchaveza.cinepolis.data.net.callbacks;

import android.content.Context;

import com.example.mchaveza.cinepolis.data.net.base.BusProvider;
import com.example.mchaveza.cinepolis.data.net.events.SendComplexEvent;
import com.example.mchaveza.cinepolis.data.net.models.ComplexResponse;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by knock on 01/04/2017.
 */

public class ComplexCallback {

    public static Callback<List<ComplexResponse>> complexCallback(Context context){
        return new Callback<List<ComplexResponse>>() {
            @Override
            public void onResponse(Call<List<ComplexResponse>> call, Response<List<ComplexResponse>> response) {
                BusProvider.getInstance().post(new SendComplexEvent(true, null, response.body()));
            }

            @Override
            public void onFailure(Call<List<ComplexResponse>> call, Throwable t) {
                BusProvider.getInstance().post(new SendComplexEvent(false, t.getMessage(), null));
            }
        };
    }

}

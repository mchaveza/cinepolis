package com.example.mchaveza.cinepolis.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mchaveza.cinepolis.ui.fragments.MoviesDescriptionFragment;
import com.example.mchaveza.cinepolis.ui.fragments.MoviesPreviewFragment;
import com.example.mchaveza.cinepolis.ui.fragments.MoviesShowtimesFragment;

/**
 * Created by knock on 02/04/2017.
 */

public class SectionsPageAdapter extends FragmentPagerAdapter {

    public SectionsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return MoviesShowtimesFragment.newInstance();

            case 1:
                return MoviesPreviewFragment.newInstance();

            case 2:
                return MoviesDescriptionFragment.newInstance();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SHOWTIMES";
            case 1:
                return "PREVIEW";
            case 2:
                return "DESCRIPTION";
        }
        return null;
    }

}

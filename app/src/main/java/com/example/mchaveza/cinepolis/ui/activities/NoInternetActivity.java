package com.example.mchaveza.cinepolis.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.example.mchaveza.cinepolis.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by knock on 04/04/2017.
 */

public class NoInternetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_no_internet);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.noInternet_ok)
    public void onGetOut(){
        finish();
    }
}

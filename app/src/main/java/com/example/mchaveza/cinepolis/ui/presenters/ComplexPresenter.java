package com.example.mchaveza.cinepolis.ui.presenters;

import com.example.mchaveza.cinepolis.data.net.events.GetComplexEvent;
import com.example.mchaveza.cinepolis.data.net.events.SendComplexEvent;
import com.example.mchaveza.cinepolis.ui.views.ComplexView;
import com.squareup.otto.Subscribe;

/**
 * Created by knock on 01/04/2017.
 */

public class ComplexPresenter extends BasePresenter {

    private ComplexView mListener;

    public ComplexPresenter(ComplexView mListener) {
        register();
        this.mListener = mListener;
    }

    public void getComplex(int[] ids){
        mListener.showLoading();
        mBus.post(new GetComplexEvent(ids));
    }

    @Subscribe
    public void onGetComplexEvent(SendComplexEvent sendComplexEvent){
        mListener.hideLoading();
        if(sendComplexEvent.isSuccess()){
            mListener.onGetComplex(sendComplexEvent.getResponse());
        }else{
            mListener.onGetComplexError(sendComplexEvent.getMessage());
        }
    }
}
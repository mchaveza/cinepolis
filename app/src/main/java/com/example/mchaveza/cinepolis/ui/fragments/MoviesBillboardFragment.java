package com.example.mchaveza.cinepolis.ui.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.data.local.HorariosModel;
import com.example.mchaveza.cinepolis.ui.adapters.HorariosRecyclerAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;
import com.example.mchaveza.cinepolis.ui.utils.DBContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by knock on 10/04/2017.
 */

public class MoviesBillboardFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_ID = 5;

    private long idPelicula;
    private String idComplejoVista;

    public static MoviesBillboardFragment newInstance() {
        MoviesBillboardFragment fragment = new MoviesBillboardFragment();

        return fragment;
    }

    @BindView(R.id.showtimes_recycler_date)
    RecyclerView mRecycler;

    private LinearLayout recyclerView;
    private LinearLayout notAvailable;
    private HorariosRecyclerAdapter mAdapter;
    private List<HorariosModel> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_showtimes, container, false);
        recyclerView = (LinearLayout) view.findViewById(R.id.showtimes_recyclers);
        notAvailable = (LinearLayout) view.findViewById(R.id.showtimes_not_available);
        getLoaderManager().initLoader(LOADER_ID, null, this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        idPelicula = MoviesTabbedFragment.id;
        idComplejoVista = MoviesTabbedFragment.idComplejoVista;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = new String[]{
                DBContract.CarteleraEntry.COLUMN_CARTELERA_ID_PELICULA,
                DBContract.CarteleraEntry.COLUMN_CARTELERA_ID_COMPLEJO_VISTA,
                DBContract.CarteleraEntry.COLUMN_CARTELERA_HORARIO,
                DBContract.CarteleraEntry.COLUMN_CARTELERA_FECHA
        };

        return new CursorLoader(getActivity(), DBContract.CarteleraEntry.CONTENT_URI, projection, null, null, DBContract.CarteleraEntry.COLUMN_CARTELERA_FECHA);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        list.clear();
        while (data.moveToNext()) {

            int idxIdPelicula = data.getColumnIndex(DBContract.CarteleraEntry.COLUMN_CARTELERA_ID_PELICULA);
            int idxIdComplejoVista = data.getColumnIndex(DBContract.CarteleraEntry.COLUMN_CARTELERA_ID_COMPLEJO_VISTA);
            int idxHorario = data.getColumnIndex(DBContract.CarteleraEntry.COLUMN_CARTELERA_HORARIO);
            int idxFecha = data.getColumnIndex(DBContract.CarteleraEntry.COLUMN_CARTELERA_FECHA);

            if (data.getLong(idxIdPelicula) == idPelicula && data.getString(idxIdComplejoVista).equals(idComplejoVista)) {

                HorariosModel model = new HorariosModel();
                model.setFecha(data.getString(idxFecha));
                model.setHorario(data.getString(idxHorario));
                list.add(model);

            }
        }

        list = groupByDay(list);
        if(list.size() == 0){
            recyclerView.setVisibility(View.GONE);
            notAvailable.setVisibility(View.VISIBLE);
        }

        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.setHasFixedSize(true);
        mAdapter = new HorariosRecyclerAdapter(getActivity(), list);
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private List<HorariosModel> groupByDay(List<HorariosModel> list){
        HorariosModel horariosModel = new HorariosModel();
        String groupedDates = "";
        List<HorariosModel> listArranged = new ArrayList<>();
        for(int i = 0, superIndex = 0; i < list.size(); i++){
            if(i == superIndex){
                if((i + 1) != list.size()){
                    groupedDates += list.get(i).getHorario() + "\n";
                }else{
                    groupedDates += list.get(i).getHorario() + "\n";
                    horariosModel.setFecha(list.get(superIndex).getFecha());
                    horariosModel.setHorario(groupedDates);
                    superIndex = i;
                    listArranged.add(horariosModel);
                    groupedDates = "";
                    horariosModel = new HorariosModel();
                }

            }else{
                if(list.get(i).getFecha().equals(list.get(superIndex).getFecha())){
                    if((i + 1) != list.size()){
                        groupedDates += list.get(i).getHorario() + "\n";
                    }else{
                        groupedDates += list.get(i).getHorario() + "\n";
                        horariosModel.setFecha(list.get(superIndex).getFecha());
                        horariosModel.setHorario(groupedDates);
                        superIndex = i;
                        listArranged.add(horariosModel);
                        groupedDates = "";
                    }

                }else{
                    groupedDates += list.get(i).getHorario() + "\n";
                    horariosModel.setFecha(list.get(superIndex).getFecha());
                    horariosModel.setHorario(groupedDates);
                    superIndex = i;
                    listArranged.add(horariosModel);
                    groupedDates = "";
                    horariosModel = new HorariosModel();
                }
            }
        }

        return listArranged;
    }
}

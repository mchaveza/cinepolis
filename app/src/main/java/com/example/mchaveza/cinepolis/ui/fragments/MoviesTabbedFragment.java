package com.example.mchaveza.cinepolis.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.adapters.SectionsPageAdapter;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;

import butterknife.BindView;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesTabbedFragment extends BaseFragment {

    private static String ARG_MOVIE_ID = "movie_id";
    private static String ARG_MOVIE_TITLE = "movie_title";
    private static String ARG_MOVIE_GENRE = "movie_genre";
    private static String ARG_MOVIE_CLASIFICATION = "movie_clasification";
    private static String ARG_MOVIE_DURATION = "movie_duration";
    private static String ARG_MOVIE_DIRECTOR = "movie_director";
    private static String ARG_MOVIE_CAST = "movie_cast";
    private static String ARG_MOVIE_SYNOPSIS = "movie_synopsis";
    private static String ARG_MOVIE_IMAGE = "movie_image";
    public static String ARG_ID_COMPLEJO_VISTA = "id_complejo_vista";

    public static int id;
    public static String idComplejoVista;
    public static String titulo;
    public static String genero;
    public static String clasificacion;
    public static String duracion;
    public static String director;
    public static String actores;
    public static String sinopsis;
    public static String imagenCartel;

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;
    private static Intent getComplexIntent;


    public static MoviesTabbedFragment newInstance(Intent intent) {
        MoviesTabbedFragment fragment = new MoviesTabbedFragment();
        getComplexIntent = intent;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);

        mSectionsPageAdapter = new SectionsPageAdapter(getFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.tabbed_container);
        mViewPager.setAdapter(mSectionsPageAdapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        getMoviesExtras();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void getMoviesExtras(){
        titulo = getComplexIntent.getStringExtra(ARG_MOVIE_TITLE);
        id = getComplexIntent.getIntExtra(ARG_MOVIE_ID, 0);
        genero = getComplexIntent.getStringExtra(ARG_MOVIE_GENRE);
        clasificacion = getComplexIntent.getStringExtra(ARG_MOVIE_CLASIFICATION);
        duracion = getComplexIntent.getStringExtra(ARG_MOVIE_DURATION);
        director = getComplexIntent.getStringExtra(ARG_MOVIE_DIRECTOR);
        actores = getComplexIntent.getStringExtra(ARG_MOVIE_CAST);
        sinopsis = getComplexIntent.getStringExtra(ARG_MOVIE_SYNOPSIS);
        imagenCartel = getComplexIntent.getStringExtra(ARG_MOVIE_IMAGE);
        idComplejoVista = getComplexIntent.getStringExtra(ARG_ID_COMPLEJO_VISTA);

    }
}

package com.example.mchaveza.cinepolis.data.local;

/**
 * Created by knock on 09/04/2017.
 */

public class PeliculasDescripcionModel {

    int id;
    String titulo;
    String genero;
    String clasificacion;
    String duracion;
    String director;
    String actores;
    String sinopsis;
    String imagenCartel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getImagenCartel() {
        return imagenCartel;
    }

    public void setImagenCartel(String imagenCartel) {
        this.imagenCartel = imagenCartel;
    }
}

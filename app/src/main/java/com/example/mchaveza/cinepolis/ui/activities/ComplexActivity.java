package com.example.mchaveza.cinepolis.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.fragments.ComplexFragment;

/**
 * Created by knock on 03/04/2017.
 */

public class ComplexActivity extends BaseActivity {

    public static String ARG_NOMBRE = "nombre";
    public static String ARG_ID_COMPLEJO_VISTA = "id_complejo_vista";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intentFromMaps = getIntent();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, ComplexFragment.newInstance(intentFromMaps.getStringExtra(ARG_ID_COMPLEJO_VISTA)))
                    .commit();
        }
        mTitle.setText(intentFromMaps.getStringExtra(ARG_NOMBRE));
        mTitle.setTextSize(getResources().getDimension(R.dimen.complex_title_text_size));
    }
}

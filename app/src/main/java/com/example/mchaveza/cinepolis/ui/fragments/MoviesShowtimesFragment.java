package com.example.mchaveza.cinepolis.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.base.BaseFragment;

/**
 * Created by knock on 02/04/2017.
 */

public class MoviesShowtimesFragment extends MoviesTabbedFragment{

    public static MoviesShowtimesFragment newInstance(){
        MoviesShowtimesFragment fragment = new MoviesShowtimesFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.container_movies_showtimes, container, false);

        com.example.mchaveza.moviedescriptionmodule.models.PeliculasDescripcionModel model = new
                com.example.mchaveza.moviedescriptionmodule.models.PeliculasDescripcionModel();
        model.setImagenCartel(imagenCartel);
        model.setTitulo(titulo);
        model.setGenero(genero);
        model.setClasificacion(clasificacion);
        model.setDuracion(duracion);
        model.setId(id);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_showtimes_description, new com.example.mchaveza.moviedescriptionmodule.fragments.MoviesDescriptionFragmentModule().newInstance(model))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_showtimes, new MoviesBillboardFragment().newInstance())
                .commit();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}

package com.example.mchaveza.cinepolis.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.mchaveza.cinepolis.R;
import com.example.mchaveza.cinepolis.ui.base.BaseActivity;
import com.example.mchaveza.cinepolis.ui.fragments.MapsFragment;

/**
 * Created by knock on 31/03/2017.
 */

public class MapsActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, MapsFragment.newInstance())
                    .commit();
        }
        mTitle.setText(getString(R.string.section_maps));
    }
}

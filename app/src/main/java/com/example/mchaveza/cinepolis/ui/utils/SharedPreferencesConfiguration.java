package com.example.mchaveza.cinepolis.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by knock on 13/04/2017.
 */

public class SharedPreferencesConfiguration {

    private Context mContext;
    public static String ARG_PERMISSION_CHECK = "permission_location";
    public static SharedPreferences sharedPreferences;

    public SharedPreferencesConfiguration(Context context) {
        mContext = context;
    }

    public boolean getSharedPreferences() {
        Boolean permissionGranted;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        permissionGranted = sharedPreferences.getBoolean(ARG_PERMISSION_CHECK, false);
        return permissionGranted;
    }

    public void setSharedPreferences(Boolean permissionGranted) {
        sharedPreferences.edit().putBoolean(ARG_PERMISSION_CHECK, permissionGranted).commit();
        sharedPreferences.edit().putBoolean(ARG_PERMISSION_CHECK, permissionGranted).apply();
    }

}

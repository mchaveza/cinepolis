package widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by knock on 08/04/2017.
 */

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    private String font;

    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        Typeface typeface = null;
        if(getTypeface() != null && getTypeface().getStyle() == Typeface.ITALIC){
            typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Ginóra Sans Oblique.otf");
        }else{
            if(getTypeface() != null && getTypeface().getStyle() == Typeface.BOLD){
                typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Ginóra Sans Bold.otf");
            }else{
                typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Ginóra Sans Regular.otf");
            }

        }


        setTypeface(typeface);
    }

}

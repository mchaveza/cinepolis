package com.example.mchaveza.moviedescriptionmodule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mchaveza.moviedescriptionmodule.R;
import com.example.mchaveza.moviedescriptionmodule.models.PeliculasDescripcionModel;

/**
 * Created by knock on 10/04/2017.
 */

public class MoviesDescriptionFragmentModule extends Fragment {

    long idMovie;
    private static String ARG_ID_MOVIE = "movie_id";
    private static String ARG_ID_TITLE = "movie_title";
    private static String ARG_ID_DURATION = "movie_duration";
    private static String ARG_ID_GENRE = "movie_genre";
    private static String ARG_ID_CLASIFICATION = "movie_clasification";
    private static String ARG_ID_IMAGE_CARTEL = "movie_image";

    private String movieTitle;
    private String movieDuration;
    private String movieClasification;
    private String movieGenre;
    private String movieImage;


    public MoviesDescriptionFragmentModule() {

    }

    public static MoviesDescriptionFragmentModule newInstance(PeliculasDescripcionModel model) {
        MoviesDescriptionFragmentModule fragment = new MoviesDescriptionFragmentModule();
        Bundle extras = new Bundle();
        extras.putLong(ARG_ID_MOVIE, model.getId());
        extras.putString(ARG_ID_TITLE, model.getTitulo());
        extras.putString(ARG_ID_DURATION, model.getDuracion());
        extras.putString(ARG_ID_GENRE, model.getGenero());
        extras.putString(ARG_ID_CLASIFICATION, model.getClasificacion());
        extras.putString(ARG_ID_IMAGE_CARTEL, model.getImagenCartel());
        fragment.setArguments(extras);
        return fragment;

    }

    ImageView image;
    TextView title;
    TextView genre;
    TextView clasification;
    TextView duration;
    TextView genreIndicator;
    TextView clasificationIndicator;
    TextView durationIndicator;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            idMovie = getArguments().getLong(ARG_ID_MOVIE);
            movieTitle = getArguments().getString(ARG_ID_TITLE);
            movieGenre = getArguments().getString(ARG_ID_GENRE);
            movieClasification = getArguments().getString(ARG_ID_CLASIFICATION);
            movieDuration = getArguments().getString(ARG_ID_DURATION);
            movieImage = getArguments().getString(ARG_ID_IMAGE_CARTEL);

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_description_module, container, false);

        title = (TextView) view.findViewById(R.id.movie_description_title);
        genre = (TextView) view.findViewById(R.id.movie_description_genre);
        clasification = (TextView) view.findViewById(R.id.movie_description_clasification);
        duration = (TextView) view.findViewById(R.id.movie_description_duration);
        image = (ImageView) view.findViewById(R.id.movie_description_image);
        genreIndicator = (TextView) view.findViewById(R.id.movie_description_genre_indicator);
        clasificationIndicator = (TextView) view.findViewById(R.id.movie_description_clasification_indicator);
        durationIndicator = (TextView) view.findViewById(R.id.movie_description_duration_indicator);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title.setText(movieTitle);
        Glide.with(getActivity())
                .load(movieImage)
                .into(image);

        if (!movieDuration.isEmpty()) {
            durationIndicator.setVisibility(View.VISIBLE);
            duration.setText(movieDuration);
        }

        if (!movieClasification.isEmpty()) {
            clasificationIndicator.setVisibility(View.VISIBLE);
            clasification.setText(movieClasification);
        }

        if (!movieGenre.isEmpty()) {
            genreIndicator.setVisibility(View.VISIBLE);
            genre.setText(movieGenre);
        }


    }
}
